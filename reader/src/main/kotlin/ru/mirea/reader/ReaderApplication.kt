package ru.mirea.reader

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.core.io.InputStreamResource
import ru.mirea.utils.extensionProperty

@SpringBootApplication
class ReaderApplication

fun main(args: Array<String>) {
    runApplication<ReaderApplication>(*args)
}

object ExtensionProperties {
    var InputStreamResource.name: String? by extensionProperty()
}

package ru.mirea.reader.service

import io.grpc.Metadata
import io.grpc.stub.MetadataUtils
import io.grpc.stub.StreamObserver
import net.devh.boot.grpc.client.inject.GrpcClient
import org.springframework.core.io.InputStreamResource
import org.springframework.stereotype.Service
import ru.mirea.proto.Constant
import ru.mirea.proto.lib.ProcessRecordsServiceGrpc
import ru.mirea.proto.process.lib.ProcessRecordsRequest
import ru.mirea.proto.process.lib.ProcessRecordsResponse
import ru.mirea.proto.process.lib.RawRow
import ru.mirea.proto.utils.lib.FileMetadata
import ru.mirea.proto.utils.lib.ResponseStatus
import ru.mirea.reader.ExtensionProperties.name
import ru.mirea.utils.log
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicLong


@Service
class ReadFileServiceImpl : ReadFileService {

    @GrpcClient(value = "processor-1")
    private lateinit var processor1: ProcessRecordsServiceGrpc.ProcessRecordsServiceStub

    @GrpcClient(value = "processor-2")
    private lateinit var processor2: ProcessRecordsServiceGrpc.ProcessRecordsServiceStub

    @GrpcClient(value = "processor-3")
    private lateinit var processor3: ProcessRecordsServiceGrpc.ProcessRecordsServiceStub

    @GrpcClient(value = "processor-4")
    private lateinit var processor4: ProcessRecordsServiceGrpc.ProcessRecordsServiceStub

    companion object {
        private const val TAG: String = "ru.mirea.reader.service.ReadFileServiceImplKt"
    }

    private var readFileRequestStreamObservers: List<StreamObserver<ProcessRecordsRequest>>? = null
    private var metadata: Metadata? = null
    private var processors: List<ProcessRecordsServiceGrpc.ProcessRecordsServiceStub>? = null
    private var countDownLatch: CountDownLatch? = null
    private var fileName: String? = null

    private val response = StringBuilder()

    private val isFirstLine = AtomicBoolean(true)
    private val currentSentRecordCount = AtomicLong(0L)
    private val overallSentRecordCount = AtomicLong(0L)
    private val receivedProcessedRecordCount = AtomicLong(0L)

    override fun readFile(inputStreamResource: InputStreamResource): String {
        fileName = inputStreamResource.name
        processors = listOf(processor1, processor2, processor3, processor4)
        countDownLatch = CountDownLatch(1)
        response.clear()

        val startTime = System.nanoTime()
        inputStreamResource.inputStream.use { inputStream ->
            val bufferedReader = BufferedReader(InputStreamReader(inputStream, "UTF-8"))

            try {
                val records = ArrayList<String>()
                var totalBatchCount = 0L

                bufferedReader.lines().forEach { line ->
                    if (isFirstLine.getAndSet(false)) return@forEach setupGRPC(line)

                    if (records.add(line) && records.size == 10) {
                        val request = ProcessRecordsRequest
                            .newBuilder()
                            .addAllRecords(
                                records.map { row ->
                                    RawRow.newBuilder()
                                        .setValue(row)
                                        .build()
                                }
                            )
                            .build()

                        // sending the request that contains 10 records (rows) of file (csv) to processors
                        readFileRequestStreamObservers!![(totalBatchCount % processors!!.size.toLong()).toInt()].onNext(request)

                        /**
                         * TODO: Figure out how to properly handle spam of records, causing the CPU/memory (hardware) starvation
                         */
                        Thread.sleep(1)

                        currentSentRecordCount.addAndGet(records.size.toLong())
                        records.clear()

                        ++totalBatchCount
                    }
                }

                log(TAG, "Total number of rows that have been read: ${currentSentRecordCount.get()} rows")
            } catch (e: Exception) {
                response.append(TAG, ResponseStatus.FAILED.toString())
                e.printStackTrace()
            } finally {
                bufferedReader.close()
                readFileRequestStreamObservers!!.forEach { it.onCompleted() }
            }

            overallSentRecordCount.addAndGet(currentSentRecordCount.get())

            response.append(
                "\nOverall sent records count: ${
                    overallSentRecordCount
                }.\nCurrently sent records count: $currentSentRecordCount"
            )

            readFileRequestStreamObservers = null

            currentSentRecordCount.set(0L)
            isFirstLine.set(true)
        }

        val endTime = System.nanoTime()
        val elapsedTimeInMillis = TimeUnit.MILLISECONDS.convert((endTime - startTime), TimeUnit.NANOSECONDS)
        log(TAG, "Total elapsed time: $elapsedTimeInMillis ms")

        return response.toString()
    }

    private fun setupGRPC(headers: String) {
        log(TAG, headers)

        metadata = Metadata().apply {
            put(
                Constant.fileMetadataKey,
                FileMetadata
                    .newBuilder()
                    .setFileName(fileName)
                    .setProcessorsCount(1)
                    .addAllHeader(
                        headers
                            .split(",")
                            .map { it.trim() }
                    )
                    .build()
                    .toByteArray()
            )
        }

        readFileRequestStreamObservers = processors!!.map { processor ->
            processor
                .withInterceptors(MetadataUtils.newAttachHeadersInterceptor(metadata))
                .processRecords(
                    object : StreamObserver<ProcessRecordsResponse> {
                        override fun onNext(processRecordResponse: ProcessRecordsResponse) {
                            // called when processor sends the response
                            receivedProcessedRecordCount.addAndGet(processRecordResponse.currentReceivedRecordCount)

                            log(TAG,
                                "Processor ${
                                    processRecordResponse.processorId
                                } logs:\nFinished with status: ${
                                    processRecordResponse.status
                                }.\nOverall received records by the processor: ${
                                    processRecordResponse.overallReceivedRecordCount
                                }.\nOverall processed records  by the processor: ${
                                    processRecordResponse.overallProcessedRecordCount
                                }.\n"
                            )
                        }

                        override fun onError(t: Throwable) {
                            // called when processor sends any error
                            log(TAG, "${ResponseStatus.FAILED}\n")
                            t.printStackTrace()
                            countDownLatch!!.countDown()
                        }

                        override fun onCompleted() {
                            // called when processor finishes serving the request
                            countDownLatch!!.countDown()
                        }
                    }
                )
        }

        log(TAG, "gRPC stuff initialized")
    }
}
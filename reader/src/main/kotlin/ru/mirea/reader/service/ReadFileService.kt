package ru.mirea.reader.service

import org.springframework.core.io.InputStreamResource

interface ReadFileService {

    fun readFile(inputStreamResource: InputStreamResource): String
}
package ru.mirea.reader.controller

import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.mirea.reader.ExtensionProperties.name
import ru.mirea.reader.service.ReadFileService


@RestController
@RequestMapping("/api")
class ReadFileController (
    private val readFileService: ReadFileService
) {

    @RequestMapping("/upload")
    fun readFile(
        @RequestHeader httpHeaders: HttpHeaders,
        @RequestBody inputBodyStreamResource: InputStreamResource
    ): String =
        when {
            httpHeaders.contentType.toString() == "text/csv" -> {
                inputBodyStreamResource.name = httpHeaders.getFirst("File-Name") ?: "custom_file_name"

                readFileService.readFile(inputBodyStreamResource)
            }
            else ->
                throw IllegalArgumentException("Unsupported content type")
        }

}

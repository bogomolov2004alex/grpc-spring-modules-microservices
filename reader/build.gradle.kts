plugins {
    id("org.springframework.boot")

    kotlin("jvm")
    kotlin("plugin.spring")
}

dependencies {
    // Custom libraries
    implementation(project(":proto"))
    implementation(project(":utils"))

    // Kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.1")
    runtimeOnly("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.8.1")

    // Spring boot
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-webflux:3.2.5")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    // Reactive Streams
    implementation("org.reactivestreams:reactive-streams:1.0.4")

    // gRPC
    implementation("net.devh:grpc-client-spring-boot-starter:2.15.0.RELEASE")
}

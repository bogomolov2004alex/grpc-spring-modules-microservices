plugins {
    id("org.gradle.toolchains.foojay-resolver-convention") version "0.5.0"
}
rootProject.name = "grpc-spring-modules-microservices"
include("reader")
include("writer")
include("proto")
include("processor")
include("utils")

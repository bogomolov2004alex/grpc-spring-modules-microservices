package ru.mirea.processor.model

import ru.mirea.utils.log


class AgeOutlineRemovalModel {

    companion object {
        private const val TAG: String = "ru.mirea.reader.service.ReadFileServiceImplKt"
    }

    private val targetIndex = records[0].indexOf("72.0") // Temperature(F)

    private val data = records.map { it[targetIndex].toDouble() }
    private var q1: Double = 0.0
    private var q3: Double = 0.0
    private var IQR: Double = 0.0

//    private val targetIndex = testRecords[0].lastIndex // "Game Length"

//    private val data = testRecords.map { it[targetIndex].toInt() }
//    private var q1: Int = 0
//    private var q3: Int = 0
//    private var IQR: Int = 0

    private var lowerBound: Double = 0.0
    private var upperBound: Double = 0.0

    init {
        val sortedData = data.sorted()
        val q1Index = (sortedData.size * 0.25).toInt()
        val q3Index = (sortedData.size * 0.75).toInt()

        q1 = sortedData[q1Index]
        q3 = sortedData[q3Index]
        IQR = q3 - q1
        lowerBound = q1 - 1.5 * IQR
        upperBound = q3 + 1.5 * IQR

        log(TAG, "Lower bound: $lowerBound, Upper bound: $upperBound")
    }

    fun getArrayOfRemovedValues(records: List<List<String>>): List<Int> =
        records.map { it[targetIndex].toInt() }.filter { it.toDouble() in lowerBound..upperBound }

    fun getObjectWithRemovedOutliers(records: List<List<String>>): List<List<String>> =
        records.filter { it[targetIndex].isNotEmpty() && it[targetIndex].toDouble() in lowerBound..upperBound }
}

/**
 * Todo: Make a dynamic model, or at least implement the choice method
 *
 * First records list is for the large US_Accidents_March23.csv file
 * Second records list is for the small snakes_count_10000.csv file for testing
 */
private val records = listOf(
    listOf("A-1", "Source2", "3", "2016-02-08 05:46:00", "2016-02-08 11:00:00", "39.865147", "-84.058723", "", "", "0.01", "Accident on Old Sonoma Rd at Buhman Ave.", "Old Sonoma Rd", "Napa", "Napa", "CA", "94559-9707", "US", "US/Pacific", "KAPC", "2016-09-15 16:54:00",
        "72.0", "", "57.0", "29.94","10.0","SSW","16.1","","Clear","False","False","False","False","False","False","False","False","False","False","False","False","False","Day","Day","Day","Day"),
    listOf("A-2", "Source2", "2", "2016-02-08 06:07:59", "2016-02-08 06:37:59", "39.92805900000001", "-82.831184", "", "", "0.01", "Right hand shoulder blocked due to accident on Antelope Rd at Watt Ave.","Watt Ave","North Highlands","Sacramento","CA","95660-2609","US","US/Pacific","KMCC","2016-09-15 17:15:00",
        "82.63","","19.0","29.93","9.0","SSE","3.5","","Clear","False","False","False","False","False","False","False","False","False","False","False","True","False","Day","Day","Day","Day"),
    listOf("A-3", "Source2", "2", "2016-02-08 12:41:08", "2016-02-08 13:11:08", "40.158024", "-82.641762", "", "", "1.32", "Accident on Middlefield Rd at Hurlingame Ave.","Hurlingame Ave","Redwood City","San Mateo","CA","94063-3408","US","US/Pacific","KSQL","2016-09-15 17:47:00",
        "68.0","","60.0","29.99","10.0","NNW","6.9","","Clear","False","False","True","False","False","False","True","False","False","True","False","False","False","Day","Day","Day","Day"),
    listOf("A-4", "Source2", "2", "2016-02-08 06:49:27", "2016-02-08 07:19:27", "39.063148", "-84.032608", "", "", "0.01", "Right hand shoulder blocked due to accident on I-80 Westbound at Exit 92 Winters St.","Winters St","Sacramento","Sacramento","CA","95838","US","US/Pacific","KMCC","2016-09-15 18:15:00",
        "81.3","","18.0","29.92","9.0","NW","3.5","","Clear","False","False","False","False","False","False","False","False","False","False","False","False","False","Day","Day","Day","Day"),
    listOf("A-5", "Source2", "3", "2016-02-08 07:44:26", "2016-02-08 08:14:26", "40.100590000000004", "-82.92519399999998", "", "", "0.01", "Accident on US-101 Northbound at Exit 412 Ralston Ave.","US-101 N","Belmont","San Mateo","CA","94002","US","US/Pacific","KSQL","2016-09-15 18:47:00",
        "64.4","","73.0","30.0","10.0","North","8.1","","Clear","False","False","False","False","False","False","False","False","False","False","False","False","False","Day","Day","Day","Day"),
    listOf("A-6", "Source2", "2", "2016-02-08 07:59:35", "2016-02-08 08:29:35", "39.758274", "-84.23050699999997", "", "", "0.0", "Accident on Green Valley Rd at Hi Grade Ln.","Green Valley Rd","Freedom","Santa Cruz","CA","95019-3140","US","US/Pacific","KWVI","2016-09-15 18:53:00",
        "59.0","","90.0","29.99","10.0","ESE","9.2","","Clear","False","False","False","False","False","False","False","False","False","False","False","False","False","Day","Day","Day","Day"),
    listOf("A-7", "Source2", "3", "2016-02-08 07:59:58", "2016-02-08 08:29:58", "39.770382", "-84.194901", "", "", "0.01", "Accident on I-880 Northbound at Exit 10 California Cir.","I-880 N","Fremont","Alameda","CA","94538","US","US/Pacific","KPAO","2016-09-15 19:47:00",
        "60.8","","88.0","29.99","10.0","NNW","10.4","","Clear","False","False","False","False","False","False","False","False","False","False","False","False","False","Night","Day","Day","Day"),
    listOf("A-8", "Source2", "2", "2016-02-08 08:00:40", "2016-02-08 08:30:40", "39.778061", "-84.172005", "", "", "0.0", "Accident on High St at I-880.","I-880 N","Oakland","Alameda","CA","94601","US","US/Pacific","KOAK","2016-09-15 19:53:00",
        "60.1","","83.0","30.0","10.0","West","9.2","","Partly Cloudy","False","False","False","False","False","False","False","False","False","False","False","False","False","Night","Night","Day","Day"),
    listOf("A-9", "Source2", "3", "2016-02-08 08:10:04", "2016-02-08 08:40:04", "40.100590000000004", "-82.92519399999998", "", "", "0.01", "Accident on I-880 Northbound at Exit 23 Alvarado Niles Rd.","I-880 N","Union City","Alameda","CA","94587","US","US/Pacific","KHWD","2016-09-15 19:54:00",
        "59.0","","78.0","30.02","10.0","WNW","6.9","","Clear","False","False","False","False","True","False","False","False","False","False","False","False","False","Night","Night","Night","Day"),
    listOf("A-10", "Source2", "3", "2016-02-08 08:21:27", "2016-02-08 08:51:27", "39.932709", "-82.83091", "", "", "0.01", "Accident on US-101 Northbound at Exit 487 Corby Ave.","US-101 N","Santa Rosa","Sonoma","CA","95407","US","US/Pacific","KSTS","2016-09-15 20:53:00",
        "54.0","","90.0","29.97","10.0","SSE","5.8","","Clear","False","False","False","False","False","False","False","False","False","False","False","False","False","Night","Night","Night","Day")
)

private val testRecords = listOf(
    listOf("1", "30"),
    listOf("2", "29"),
    listOf("3", "31"),
    listOf("4", "16"),
    listOf("5", "24"),
    listOf("6", "29"),
    listOf("7", "28"),
    listOf("8", "117"),
    listOf("9", "42"),
    listOf("10", "23")
)


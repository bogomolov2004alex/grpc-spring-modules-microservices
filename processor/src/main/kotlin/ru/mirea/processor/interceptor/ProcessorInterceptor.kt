package ru.mirea.processor.interceptor

import com.google.protobuf.InvalidProtocolBufferException
import io.grpc.*
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor
import ru.mirea.proto.Constant
import ru.mirea.proto.utils.lib.FileMetadata

@GrpcGlobalServerInterceptor
class ProcessorInterceptor : ServerInterceptor {
    override fun <ReqT : Any?, RespT : Any?> interceptCall(
        serverCall: ServerCall<ReqT, RespT>?,
        metadata: Metadata?,
        serverCallHandler: ServerCallHandler<ReqT, RespT>?
    ): ServerCall.Listener<ReqT> {
        if (metadata != null && metadata.containsKey(Constant.fileMetadataKey)) {
            var fileMetadata: FileMetadata? = null

            try {
                fileMetadata = FileMetadata.parseFrom(metadata[Constant.fileMetadataKey])
            } catch (e: InvalidProtocolBufferException) {
                val status = Status.INTERNAL.withDescription("Unable to create file metadata object")
                serverCall?.close(status, metadata)
            }

            val context = Context.current().withValue(
                Constant.fileMetadataContext,
                fileMetadata
            )

            return Contexts.interceptCall(context, serverCall, metadata, serverCallHandler)
        }

        return object : ServerCall.Listener<ReqT>() {}
    }
}
package ru.mirea.processor.service

import io.grpc.Metadata
import io.grpc.stub.MetadataUtils
import io.grpc.stub.StreamObserver
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import net.devh.boot.grpc.client.inject.GrpcClient
import net.devh.boot.grpc.server.service.GrpcService
import ru.mirea.proto.lib.FileUploadServiceGrpc
import ru.mirea.proto.lib.ProcessRecordsServiceGrpc
import ru.mirea.proto.process.lib.ProcessRecordsRequest
import ru.mirea.proto.process.lib.ProcessRecordsResponse
import ru.mirea.processor.model.AgeOutlineRemovalModel
import ru.mirea.proto.Constant
import ru.mirea.proto.process.lib.RawRow
import ru.mirea.proto.upload.lib.FileUploadRequest
import ru.mirea.proto.upload.lib.FileUploadResponse
import ru.mirea.proto.upload.lib.ProcessedRow
import ru.mirea.proto.utils.lib.ResponseStatus
import ru.mirea.utils.log
import ru.mirea.utils.resettableLazy
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong

@GrpcService
class ProcessRecordGrpcService : ProcessRecordsServiceGrpc.ProcessRecordsServiceImplBase() {

    @GrpcClient(value = "file-writer")
    private lateinit var writerClient: FileUploadServiceGrpc.FileUploadServiceStub

    private lateinit var responseSender: () -> Unit

    companion object {
        private const val TAG: String = "ru.mirea.processor.service.ProcessRecordGrpcService"
    }

    private val ageOutlineRemovalModel: AgeOutlineRemovalModel by lazy {
        AgeOutlineRemovalModel()
    }

    private val recordsFlow: MutableSharedFlow<List<RawRow>> by lazy {
        MutableSharedFlow(extraBufferCapacity = 4194304)
    }

    private val metadata: Metadata by resettableLazy(
        resetCondition = { newMetadata.getAndSet(false) },
        initializer = {
            Metadata().apply {
                log(TAG, "New Metadata has been initialized")

                put(
                    Constant.fileMetadataKey,
                    Constant.fileMetadataContext
                        .get()
                        .toByteArray()
                )
            }
        }
    )

    private val fileUploadRequestStreamObserver: StreamObserver<FileUploadRequest> by resettableLazy(
        resetCondition = { newObserver.getAndSet(false) },
        initializer = {
            writerClient
                .withInterceptors(MetadataUtils.newAttachHeadersInterceptor(metadata))
                .uploadFile(
                    object : StreamObserver<FileUploadResponse> {

                        override fun onNext(fileUploadResponse: FileUploadResponse) {
                            // called when writer sends the response
                            log(TAG,
                                "Writer finished with status: ${
                                    fileUploadResponse.status
                                }. Records are writen by writer: ${
                                    fileUploadResponse.currentWritenRecordCount
                                }. Overall records writen by writer: ${
                                    fileUploadResponse.overallWritenRecordCount
                                }"
                            )
                        }

                        override fun onError(t: Throwable) {
                            // called when writer sends any error
                            t.printStackTrace()
                        }

                        override fun onCompleted() {
                            // called when writer finishes serving the request
                        }
                    }
                )
                .also { log(TAG, "New FileUploadRequestStreamObserver has been initialized") }
        }
    )

    private val newMetadata = AtomicBoolean(true)
    private val newObserver = AtomicBoolean(true)

    private val clientCount = AtomicInteger(0)
    private val completeClientCount = AtomicInteger(0)

    // Values for logs and control of data (records)
    private val currentProcessedRecordCount = AtomicLong(0L)
    private val currentIncomingRecordCount = AtomicLong(0L)
    private val overallProcessedRecordCount = AtomicLong(0L)
    private val overallIncomingRecordCount = AtomicLong(0L)

    init {
        recordsFlow
            .onEach { records ->
                if (records.isEmpty()) {
                    if (clientCount.get() == completeClientCount.incrementAndGet()) {
                        log(TAG, "Completed processing. Incoming records: ${
                            currentIncomingRecordCount.get()
                        }. Client count: ${
                            clientCount.get()
                        }. Complete client count: ${
                            completeClientCount.get()
                        }")

                        withContext(Dispatchers.Default) {
                            responseSender()
                        }
                    }
                    return@onEach
                }

                currentIncomingRecordCount.addAndGet(records.size.toLong())

//                log(TAG,
//                    "Records to process: ${
//                        records.size
//                    }. Incoming records: ${
//                        currentIncomingRecordCount.get()
//                    }. Thread ID: ${
//                        Thread.currentThread().threadId()
//                    }. Client count: ${
//                        clientCount.get()
//                    }. Complete client count: ${
//                        completeClientCount.get()
//                    }"
//                )

                val processedRecords = ageOutlineRemovalModel.getObjectWithRemovedOutliers(
                    records.map {
                        it.value.replace(" ", "").split(",")
                    }
                )

                if (processedRecords.isEmpty()) {
//                    log(TAG, "List of processed records is empty")
                    return@onEach
                }

                val request = FileUploadRequest
                    .newBuilder()
                    .addAllRecords(
                        processedRecords.map { values ->
                            ProcessedRow.newBuilder()
                                .addAllValue(values)
                                .build()
                        }
                    )
                    .build()

                // sending the request that contains processed records to writer
                fileUploadRequestStreamObserver.onNext(request)

                currentProcessedRecordCount.addAndGet(processedRecords.size.toLong())
            }
            .catch { cause -> log(TAG, "Exception: $cause") }
            .launchIn(CoroutineScope(Dispatchers.IO))
    }

    override fun processRecords(responseObserver: StreamObserver<ProcessRecordsResponse>): StreamObserver<ProcessRecordsRequest> {
        if (clientCount.getAndIncrement() == 0) {
            responseSender = {
                complete {
                    responseObserver.onNext(
                        ProcessRecordsResponse.newBuilder()
                            .setProcessorId(Thread.currentThread().threadId().toInt())
                            .setCurrentReceivedRecordCount(currentIncomingRecordCount.get())
                            .setCurrentProcessedRecordCount(currentProcessedRecordCount.get())
                            .setOverallReceivedRecordCount(overallIncomingRecordCount.get())
                            .setOverallProcessedRecordCount(overallProcessedRecordCount.get())
                            .setStatus(ResponseStatus.SUCCEEDED)
                            .build()
                    )

                    log(TAG, "File has been processed. Response to the client (reader) sent")

                    responseObserver.onCompleted()
                }
            }
            log(TAG, "First client has been connected")
        }
        metadata

        return object : StreamObserver<ProcessRecordsRequest> {

            override fun onNext(processRecordsRequest: ProcessRecordsRequest) {
                // called when the reader sends the records

                recordsFlow.tryEmit(processRecordsRequest.recordsList)
            }

            override fun onError(t: Throwable?) {
                // called when reader sends the error
                log(TAG, t.toString())
            }

            override fun onCompleted() {
                // called when reader finished sending the records
                log(TAG, "Client finished sending data to the processor")
                recordsFlow.tryEmit(emptyList())
            }
        }
    }

    private fun complete(sendResponse: () -> Unit) {
        clientCount.set(0)
        completeClientCount.set(0)

        overallIncomingRecordCount.addAndGet(currentIncomingRecordCount.get())
        overallProcessedRecordCount.addAndGet(currentProcessedRecordCount.get())

        fileUploadRequestStreamObserver.onCompleted()
        sendResponse()

        currentIncomingRecordCount.set(0L)
        currentProcessedRecordCount.set(0L)

        newMetadata.set(true)
        newObserver.set(true)
    }
}
package ru.mirea.writer.utils

import ru.mirea.utils.log
import java.io.*
import java.nio.charset.StandardCharsets

class YmlWriter(
    fileName: String,
    private val headers: List<String>,
    private val parsingBy: ParsingType = ParsingType.ROW
) {

    companion object {
        private const val TAG: String = "ru.mirea.writer.utils.YmlWriterKt"
    }

    private var path: String = "D:\\Kotlin\\grpc-spring-modules-microservices\\output"
    private val completeFile = File("$path\\$fileName.yml").normalize()

    private val tmpFileNames = headers.map { it.replace(Regex("[^A-Za-z0-9\\-_.]"), "_").lowercase() }
    private val tmpFiles: List<File> = tmpFileNames.map { File("$path\\$it.yml").normalize() }

    private val completeFileWriter: BufferedWriter

    private var tmpFilesWriters: List<BufferedWriter>? = null

    init {
        if (completeFile.exists()) {
            log(TAG, "Deleting complete file \"${completeFile.name}\"...")
            completeFile.delete()
        }

        tmpFiles.forEach { file ->
            if (file.exists()) {
                log(TAG, "Deleting tmp file \"${file.name}\"...")
                file.delete()
            }
        }

        completeFile.createNewFile()

        completeFileWriter = FileOutputStream(completeFile, true).bufferedWriter(StandardCharsets.UTF_8)

        completeFileWriter.apply {
            write("ru-mirea-bogomolov:\n")
            write("  headers:\n")

            headers.forEach { header ->
                write("    - $header\n")
            }

            write("\n  content:\n")
        }

        if (parsingBy == ParsingType.COLUMN) {
            tmpFiles.forEach { file -> file.createNewFile() }
            tmpFilesWriters = tmpFiles.map { FileOutputStream(it, true).bufferedWriter(StandardCharsets.UTF_8) }

            tmpFilesWriters!!.forEachIndexed { index, bufferedWriter -> bufferedWriter.write("${headers[index]}:\n") }
        }
    }

    @Synchronized
    @Throws(IOException::class, FileNotFoundException::class)
    fun write(record: List<String>) =
        when (parsingBy) {
            ParsingType.ROW -> writeByRow(record)
            ParsingType.COLUMN -> writeByColumn(record)
        }

    @Throws(IOException::class, SecurityException::class)
    fun finishedWriting() {
        log(TAG, "Finishing writer")

        if (parsingBy == ParsingType.COLUMN) mergeTmpFiles()

        completeFileWriter.close()
    }

    @Throws(IOException::class, FileNotFoundException::class)
    private fun writeByRow(record: List<String>) {
        record.forEachIndexed { index, value ->
            completeFileWriter.write("    - ${headers[index]}: $value\n")
        }

        completeFileWriter.write("    -\n")
        completeFileWriter.flush()
    }

    @Throws(IOException::class, FileNotFoundException::class)
    private fun writeByColumn(record: List<String>) {
        record.forEachIndexed { index, value ->
            tmpFilesWriters?.get(index)?.write("  - $value\n")
            tmpFilesWriters?.get(index)?.flush()
        }
    }

    @Throws(IOException::class, SecurityException::class)
    private fun mergeTmpFiles() {
        log(TAG, "Merging...")

        tmpFilesWriters?.forEach {
            it.close()
        }

        tmpFiles.forEach { tmpFile ->
            tmpFile.bufferedReader()
                .use { reader ->
                    completeFileWriter.write("${
                        reader
                            .readText()
                            .prependIndent("    ")
                        }\n"
                    )

                    completeFileWriter.flush()
                }

            tmpFile.delete()
        }

        log(TAG, "Files merged successfully!")
    }
}

enum class ParsingType {
    ROW,
    COLUMN
}
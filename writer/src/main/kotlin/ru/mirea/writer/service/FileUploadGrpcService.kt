package ru.mirea.writer.service

import io.grpc.Status
import io.grpc.stub.StreamObserver
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import net.devh.boot.grpc.server.service.GrpcService
import ru.mirea.proto.lib.FileUploadServiceGrpc
import ru.mirea.proto.upload.lib.FileUploadRequest
import ru.mirea.proto.upload.lib.FileUploadResponse
import ru.mirea.proto.utils.lib.ResponseStatus
import ru.mirea.proto.Constant
import ru.mirea.proto.upload.lib.ProcessedRow
import ru.mirea.proto.utils.lib.FileMetadata
import ru.mirea.writer.utils.YmlWriter
import java.io.IOException
import ru.mirea.utils.log
import ru.mirea.utils.resettableLazy
import ru.mirea.writer.utils.ParsingType
import java.io.FileNotFoundException
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong

@GrpcService
class FileUploadGrpcService : FileUploadServiceGrpc.FileUploadServiceImplBase() {

    private lateinit var responseSender: () -> Unit
    private lateinit var errorSender: (String) -> Unit

    companion object {
        private const val TAG: String = "ru.mirea.writer.service.FileUploadGrpcServiceKt"
    }

    private val recordsFlow: MutableSharedFlow<List<ProcessedRow>> by lazy {
        MutableSharedFlow(extraBufferCapacity = 4194304)
    }

    private val fileMetadata: FileMetadata by resettableLazy(
        resetCondition = { newMetadata.getAndSet(false) },
        initializer = {
            Constant.fileMetadataContext
               .get()
               .also { log(TAG, "Received headers: ${it.headerList}, and file name: ${it.fileName}") }
        }
    )

    private val writer: YmlWriter by resettableLazy(
        resetCondition = { newWriter.getAndSet(false) },
        initializer = {
            YmlWriter(
                fileMetadata.fileName,
                fileMetadata.headerList,
                parsingBy = ParsingType.COLUMN
            )
        }
    )

    private val startTime: Long by resettableLazy(
        resetCondition = { newTimer.getAndSet(false) },
        initializer = { System.nanoTime() }
    )

    private val newMetadata = AtomicBoolean(true)
    private val newWriter = AtomicBoolean(true)
    private val newTimer = AtomicBoolean(true)

    private val clientCount = AtomicInteger(0)
    private val completeClientCount = AtomicInteger(0)

    // Values for logs and control of data (records)
    private val currentIncomingRecordCount = AtomicLong(0L)
    private val currentWritenRecordCount = AtomicLong(0L)
    private val overallIncomingRecordCount = AtomicLong(0L)
    private val overallWritenRecordCount = AtomicLong(0L)

    init {
        recordsFlow
            .onEach { records ->
                if (records.isEmpty()) {
                    if (clientCount.get() == completeClientCount.incrementAndGet()) {
                        log(TAG, "Completed writing. Incoming records: ${
                            currentIncomingRecordCount.get()
                        }. Client count: ${
                            clientCount.get()
                        }. Complete client count: ${
                            completeClientCount.get()
                        }")

                        withContext(Dispatchers.Default) {
                            responseSender()
                        }
                    }
                    return@onEach
                }

                currentIncomingRecordCount.addAndGet(records.size.toLong())

//                log(TAG,
//                    "Records to write: ${
//                        records.size
//                    }. Incoming records: ${
//                        currentIncomingRecordCount.get()
//                    }. Thread ID: ${
//                        Thread.currentThread().threadId()
//                    }. Client count: ${
//                        clientCount.get()
//                    }. Complete client count: ${
//                        completeClientCount.get()
//                    }"
//                )

                try {
                    startTime
                    records.forEach { record ->
                        writer.write(record.valueList)
                    }

                    currentWritenRecordCount.addAndGet(records.size.toLong())
                } catch (e: IOException) {
                    e.printStackTrace()
                    errorSender("Cannot write records due to ${e.message}")
                    return@onEach
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    errorSender("Cannot write records due to ${e.message}")
                    return@onEach
                }

                currentWritenRecordCount.addAndGet(records.size.toLong())
            }
            .catch { cause -> log(TAG, "Closing flow with exception: $cause") }
            .launchIn(CoroutineScope(Dispatchers.IO))
    }

    override fun uploadFile(responseObserver: StreamObserver<FileUploadResponse>): StreamObserver<FileUploadRequest> {
        if (clientCount.getAndIncrement() == 0) {
            errorSender = { description ->
                responseObserver.onError(Status.INTERNAL.withDescription(description).asRuntimeException())
            }
            responseSender = {
                complete {
                    responseObserver.onNext(
                        FileUploadResponse.newBuilder()
                            .setCurrentReceivedRecordCount(currentIncomingRecordCount.get())
                            .setCurrentWritenRecordCount(currentWritenRecordCount.get())
                            .setOverallWritenRecordCount(overallWritenRecordCount.get())
                            .setStatus(ResponseStatus.SUCCEEDED)
                            .build()
                    )

                    log(TAG, "File \"${fileMetadata.fileName}.yml\" has been writen. Response to the client (processor) sent")

                    responseObserver.onCompleted()
                }
            }
            log(TAG, "First client has been connected")
        }
        writer

        return object : StreamObserver<FileUploadRequest> {
            override fun onNext(fileUploadRequest: FileUploadRequest) {
                // called when the client sends the data

                recordsFlow.tryEmit(fileUploadRequest.recordsList)
            }

            override fun onError(t: Throwable?) {
                // called when client sends the error
                log(TAG, t.toString())
            }

            override fun onCompleted() {
                // called when client finished sending the data
                log(TAG, "Client finished sending data to the writer")
                recordsFlow.tryEmit(emptyList())
            }
        }
    }

    private fun complete(sendResponse: () -> Unit) {
        try {
            writer.finishedWriting()
        } catch (e: IOException) {
            errorSender("Cannot close stream(s) due to ${e.message}")
        } catch (e: SecurityException) {
            errorSender("Cannot delete file(s) due to ${e.message}")
        }

        val endTime = System.nanoTime()
        val elapsedTimeInMillis = TimeUnit.MILLISECONDS.convert((endTime - startTime), TimeUnit.NANOSECONDS)
        log(TAG, "Total elapsed time: $elapsedTimeInMillis ms")

        clientCount.set(0)
        completeClientCount.set(0)

        overallIncomingRecordCount.addAndGet(currentIncomingRecordCount.get())
        overallWritenRecordCount.addAndGet(currentWritenRecordCount.get())

        sendResponse()

        currentIncomingRecordCount.set(0L)
        currentWritenRecordCount.set(0L)

        newMetadata.set(true)
        newWriter.set(true)
        newTimer.set(true)
    }
}
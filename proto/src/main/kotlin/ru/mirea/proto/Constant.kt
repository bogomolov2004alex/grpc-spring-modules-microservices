package ru.mirea.proto

import io.grpc.Context
import io.grpc.Metadata
import ru.mirea.proto.utils.lib.FileMetadata

class Constant {

    companion object {
        val fileMetadataKey: Metadata.Key<ByteArray> = Metadata.Key.of("file-metadata-bin", Metadata.BINARY_BYTE_MARSHALLER)

        val fileMetadataContext: Context.Key<FileMetadata> = Context.key("file-metadata-context")
    }
}
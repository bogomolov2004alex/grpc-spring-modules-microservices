@echo off
setlocal enabledelayedexpansion

@rem #################################################
@rem # Script to run each module/microservice
@rem #################################################

@rem Define root directory and logs directory
set ROOT_DIR=%~dp0..

@rem Change to root directory
cd %ROOT_DIR%

@rem #################################################
@rem # Reader (1)
@rem #################################################
echo Starting reader...
start pwsh -NoExit -Command "./gradlew :reader:bootRun"

@rem #################################################
@rem # Writer (1)
@rem #################################################
echo Starting writer...
start pwsh -NoExit -Command "./gradlew :writer:bootRun"

@rem #################################################
@rem # Processor (4)
@rem #################################################

set PORTS=9091 9092 9093 9094

for %%P in (%PORTS%) do (
    echo Starting processor on port %%P
    set SPRING_APPLICATION_JSON={"grpc":{"server":{"port":%%P}}}
    start pwsh -NoExit -Command "set SPRING_APPLICATION_JSON=!SPRING_APPLICATION_JSON!; ./gradlew :processor:bootRun"
)

@rem Wait for all background processes to finish
echo All services started. Press any key to exit.
pause

@echo off

@rem #################################################
@rem # Script to build project
@rem #################################################

echo Cleaning and building the project...
./gradlew clean build

echo Build completed.
pause

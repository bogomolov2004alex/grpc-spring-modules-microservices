#!/bin/bash

#################################################
# Script to run each module/microservice
#################################################


#################################################
# Reader (1)
#################################################
echo "Starting reader..."
./gradlew :reader:bootRun


#################################################
# writer (1)
#################################################
echo "Starting writer..."
./gradlew :writer:bootRun

#################################################
# Processor (4)
#################################################

PORTS=(9091 9092 9093 9094)

for PORT in "${PORTS[@]}"; do
  echo "Starting processor on port $PORT"

  SPRING_APPLICATION_JSON="{\"grpc\":{\"server\":{\"port\":${PORT}}}}" ./gradlew :processor:bootRun &

  sleep 5
done

wait

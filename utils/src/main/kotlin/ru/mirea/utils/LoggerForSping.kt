package ru.mirea.utils

import java.util.logging.Level
import java.util.logging.LogRecord
import java.util.logging.Logger

fun log(loggerName: String, message: String) =
    Logger.getLogger(loggerName).log(LogRecord(Level.INFO, message))
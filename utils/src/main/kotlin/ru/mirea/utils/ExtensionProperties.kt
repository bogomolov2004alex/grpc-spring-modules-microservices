package ru.mirea.utils

import java.util.WeakHashMap

fun <T, R> extensionProperty() = ExtensionProperty<T, R>()

class ExtensionProperty<T, R> {
    private val map = WeakHashMap<T, R>()

    operator fun getValue(thisRef: T, property: kotlin.reflect.KProperty<*>): R? {
        return map[thisRef]
    }

    operator fun setValue(thisRef: T, property: kotlin.reflect.KProperty<*>, value: R?) {
        if (value == null) {
            map.remove(thisRef)
        } else {
            map[thisRef] = value
        }
    }
}

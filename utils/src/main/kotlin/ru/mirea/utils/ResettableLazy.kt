package ru.mirea.utils

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun <T> resettableLazy(resetCondition: () -> Boolean, initializer: () -> T) = ResettableLazy(initializer, resetCondition)

class ResettableLazy<T>(
    private var initializer: () -> T,
    private var resetCondition: () -> Boolean
) : ReadOnlyProperty<Any?, T> {
    @Volatile
    private var _value: Any? = UNINITIALIZED_VALUE

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        val _condition = resetCondition()
        val _v1 = _value
        if (_v1 !== UNINITIALIZED_VALUE && _condition.not()) {
            @Suppress("UNCHECKED_CAST")
            return _v1 as T
        }

        return synchronized(this) {
            val _v2 = _value
            if (_v2 !== UNINITIALIZED_VALUE && _condition.not()) {
                @Suppress("UNCHECKED_CAST") (_v2 as T)
            } else {
                val typedValue = initializer()
                _value = typedValue
                typedValue
            }
        }
    }

    companion object {
        private val UNINITIALIZED_VALUE = Any()
    }
}


